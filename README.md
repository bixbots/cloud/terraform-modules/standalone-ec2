# standalone-ec2

## Summary

Creates an autoscaling group of exactly one EC2 instance.

## Resources

This module creates the following resources:

* [aws_launch_configuration](https://www.terraform.io/docs/providers/aws/r/launch_configuration.html)
* [aws_autoscaling_group](https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with EC2 instances and autoscaling groups.

* AWS EC2 pricing information is available [here](https://aws.amazon.com/ec2/pricing/)

## Inputs

| Name                        | Description                                                                                                                                | Type           | Default | Required |
|:----------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:--------|:---------|
| name                        | The fully qualified name of the resource formatted as `{application}-{environment}-{role}`.                                                | `string`       | -       | yes      |
| vpc_id                      | The ID of the VPC to launch the EC2 instance into.                                                                                         | `string`       | -       | yes      |
| subnet_type                 | Specifies which subnet, either `public`, `private`, or `data`, the instance are launched in.                                               | `string`       | -       | yes      |
| instance_type               | The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG.            | `string`       | -       | yes      |
| image_id                    | ID of the AMI that should be used. If the AMI ID changes a new launch configuration will be created and added to the ASG.                  | `string`       | -       | yes      |
| spot_price                  | The maximum price to use for reserving spot instances. If omitted then spot instance will not be used.                                     | `string`       | -       | no       |
| key_name                    | Name of the SSH keypair used to access launched instances.                                                                                 | `string`       | -       | yes      |
| default_cooldown            | Amount of time, in seconds, after a scaling activity completes before another can begin.                                                   | `number`       | 300     | no       |
| health_check_grace_period   | Amount of time, in seconds, after a instance comes into service before checking the health of the instance.                                | `number`       | 300     | no       |
| rendered_cloud_init         | A list of scripts to run on the instances after boot. This is generally a rendered cloud init template but can be a single script as well. | `list(object)` | -       | no       |
| security_group_ids          | The list of security group IDs to associate with launched instances.                                                                       | `list(string)` | -       | yes      |
| iam_instance_profile_id     | The ID of the IAM instance profile to associate with launched instances.                                                                   | `string`       | -       | yes      |
| associate_public_ip_address | Whether to associate a public IP address with launched instances.                                                                          | `bool`         | `true`  | no       |
| enable_monitoring           | Whether to enable detailed monitoring with launch instances.                                                                               | `bool`         | `false` | no       |
| root_block_device           | The configuration of the root volume.                                                                                                      | `object`       | gp3/8GB | no       |
| tags                        | Additional tags to attach to all resources created by this module.                                                                         | `map(string)`  | -       | no       |

## Outputs

| Name                      | Description                           |
|:--------------------------|:--------------------------------------|
| autoscaling_group_id      | The ID of the autoscaling group.      |
| autoscaling_group_arn     | The ARN of the autoscaling group.     |
| autoscaling_group_name    | The name of the autoscaling group.    |
| launch_configuration_id   | The ID of the launch configuration.   |
| launch_configuration_arn  | The ARN of the launch configuration.  |
| launch_configuration_name | The name of the launch configuration. |

## Examples

### Simple Usage

```terraform
data "template_file" "user_data_1" {
  template = file("${path.module}/user_data_1.sh")
}

data "template_file" "user_data_2" {
  template = file("${path.module}/user_data_2.sh")
}

data "aws_vpc" "example" {
  tags = {
    "Name" = "example-env"
  }
}

data "aws_ami" "example" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "example_standalone_ec2" {
    source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2.git?ref=v1"

    subnet_type        = "public"
    vpc_id             = data.aws_vpc.example.id
    image_id           = data.aws_ami.example.id

    # omitting the "launch_pad" script for brevity
    name                    = module.launch_pad.name
    iam_instance_profile_id = module.launch_pad.iam_instance_profile
    security_group_ids      = module.launch_pad.security_group_ids

    key_name         = "example-key"
    instance_type    = "t3.micro"

    rendered_cloud_init = [{
      filename     = "user_data_1.sh"
      content_type = "text/x-shellscript"
      content      = data.template_file.user_data_1.rendered
    },{
      filename     = "user_data_2.sh"
      content_type = "text/x-shellscript"
      content      = data.template_file.user_data_2.rendered
    }]
}
```

## Version History

* v1 - Initial Release
