data "template_cloudinit_config" "cloudinit_config" {
  count = length(var.rendered_cloud_init) > 0 ? 1 : 0

  gzip          = true
  base64_encode = true

  dynamic "part" {
    for_each = [for x in var.rendered_cloud_init : x if x.filename != ""]
    content {
      filename     = part.value.filename
      content_type = part.value.content_type
      content      = part.value.content
    }
  }
}
