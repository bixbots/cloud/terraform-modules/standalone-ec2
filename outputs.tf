# autoscaling_group
output "autoscaling_group_id" {
  description = "The ID of the autoscaling group."
  value       = aws_autoscaling_group.primary.id
}
output "autoscaling_group_arn" {
  description = "The ARN of the autoscaling group."
  value       = aws_autoscaling_group.primary.arn
}
output "autoscaling_group_name" {
  description = "The name of the autoscaling group."
  value       = aws_autoscaling_group.primary.name
}

# launch_configuration
output "launch_configuration_id" {
  description = "The ID of the launch configuration."
  value       = aws_launch_configuration.primary.id
}
output "launch_configuration_arn" {
  description = "The ARN of the launch configuration."
  value       = aws_launch_configuration.primary.arn
}
output "launch_configuration_name" {
  description = "The name of the launch configuration."
  value       = aws_launch_configuration.primary.name
}
