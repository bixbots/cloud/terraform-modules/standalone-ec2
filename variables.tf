variable "name" {
  type        = string
  description = "The fully qualified name of the resource formatted as `{application}-{environment}-{role}`."
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC to launch the EC2 instance into."
}

variable "subnet_type" {
  type        = string
  description = "Specifies which subnet, either `public`, `private`, or `data`, the instance(s) are launched in."
}

variable "instance_type" {
  type        = string
  description = "The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG."
}

variable "image_id" {
  type        = string
  description = "ID of the AMI that should be used. If the AMI ID changes a new launch configuration will be created and added to the ASG."
}

variable "spot_price" {
  type        = string
  default     = ""
  description = "(Optional; Default: On-demand price) The maximum price to use for reserving spot instances. If omitted then spot instance will not be used."
}

variable "key_name" {
  type        = string
  description = "Name of the SSH keypair used to access launched instances."
}

variable "default_cooldown" {
  type        = number
  default     = 300
  description = "(Optional; Default: 300) Amount of time, in seconds, after a scaling activity completes before another can begin."
}

variable "health_check_grace_period" {
  type        = number
  default     = 300
  description = "(Optional; Default: 300) Amount of time, in seconds, after a instance comes into service before checking the health of the instance."
}

variable "rendered_cloud_init" {
  type = list(object({
    filename     = string
    content_type = string
    content      = string
  }))
  default     = []
  description = "(Optional) A list of scripts to run on the instances after boot. This is generally a rendered cloud init template but can be a single script as well."
}

variable "security_group_ids" {
  type        = list(string)
  description = "The list of security group IDs to associate with launched instances."
}

variable "iam_instance_profile_id" {
  type        = string
  description = "The ID of the IAM instance profile to associate with launched instances."
}

variable "associate_public_ip_address" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Whether to associate a public IP address with launched instances."
}

variable "enable_monitoring" {
  type        = bool
  default     = false
  description = "(Optional; Default: false) Whether to enable detailed monitoring with launch instances."
}

variable "root_block_device" {
  type = object({
    enabled     = bool
    volume_type = string
    volume_size = number
  })
  default = {
    enabled     = true
    volume_type = "gp3"
    volume_size = 8
  }
  description = "(Optional, Default: gp3/8GB) The configuration of the root volume."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
