resource "aws_launch_configuration" "primary" {
  name_prefix = "${var.name}-"

  instance_type = var.instance_type
  image_id      = var.image_id
  spot_price    = var.spot_price

  key_name          = var.key_name
  enable_monitoring = var.enable_monitoring
  user_data_base64  = length(data.template_cloudinit_config.cloudinit_config) > 0 ? data.template_cloudinit_config.cloudinit_config[0].rendered : ""

  security_groups             = var.security_group_ids
  iam_instance_profile        = var.iam_instance_profile_id
  associate_public_ip_address = var.associate_public_ip_address && var.subnet_type == "public"

  dynamic "root_block_device" {
    # Generate "root_block_device" block only if var.root_block_device.enabled is set.
    # This list will be empty if not, due to the "if" clause.
    for_each = [for x in [var.root_block_device] : x if x.enabled]
    content {
      volume_type           = root_block_device.value.volume_type
      volume_size           = root_block_device.value.volume_size
      delete_on_termination = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "primary" {
  name                 = aws_launch_configuration.primary.name
  vpc_zone_identifier  = data.aws_subnets.primary.ids
  launch_configuration = aws_launch_configuration.primary.id

  min_size = 1
  max_size = 1

  default_cooldown          = var.default_cooldown
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = "EC2"

  termination_policies = [
    "OldestLaunchConfiguration",
    "ClosestToNextInstanceHour"
  ]

  dynamic "tag" {
    for_each = var.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  tag {
    key                 = "Name"
    value               = var.name
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true

    # allow "aws_autoscaling_attachment" to be specified externally
    ignore_changes = [load_balancers, target_group_arns]
  }
}
